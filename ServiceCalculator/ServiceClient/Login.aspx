﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ServiceClient.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <meta charset="utf-8">
  
    <link rel="stylesheet" href="Styles/bootstrap.min.css">
    <link href="Styles/CustomStyles.css" rel="stylesheet" type="text/css" /
    <script src="Scripts/jquery-1.4.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body class="body-image">
    <form id="form1" runat="server">
        <div class="container">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                    </div>     
            </div>
        </div>
        </div>      
    </form>
</body>
</html>
