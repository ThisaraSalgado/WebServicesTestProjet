﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiceClient.CalculatorService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CalculatorService.ICalculatorService")]
    public interface ICalculatorService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/AddNum", ReplyAction="http://tempuri.org/ICalculatorService/AddNumResponse")]
        double AddNum(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/AddNum", ReplyAction="http://tempuri.org/ICalculatorService/AddNumResponse")]
        System.Threading.Tasks.Task<double> AddNumAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/SubNum", ReplyAction="http://tempuri.org/ICalculatorService/SubNumResponse")]
        double SubNum(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/SubNum", ReplyAction="http://tempuri.org/ICalculatorService/SubNumResponse")]
        System.Threading.Tasks.Task<double> SubNumAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/MulNum", ReplyAction="http://tempuri.org/ICalculatorService/MulNumResponse")]
        double MulNum(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/MulNum", ReplyAction="http://tempuri.org/ICalculatorService/MulNumResponse")]
        System.Threading.Tasks.Task<double> MulNumAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/DivNum", ReplyAction="http://tempuri.org/ICalculatorService/DivNumResponse")]
        double DivNum(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/DivNum", ReplyAction="http://tempuri.org/ICalculatorService/DivNumResponse")]
        System.Threading.Tasks.Task<double> DivNumAsync(double a, double b);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICalculatorServiceChannel : ServiceClient.CalculatorService.ICalculatorService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CalculatorServiceClient : System.ServiceModel.ClientBase<ServiceClient.CalculatorService.ICalculatorService>, ServiceClient.CalculatorService.ICalculatorService {
        
        public CalculatorServiceClient() {
        }
        
        public CalculatorServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CalculatorServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalculatorServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalculatorServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public double AddNum(double a, double b) {
            return base.Channel.AddNum(a, b);
        }
        
        public System.Threading.Tasks.Task<double> AddNumAsync(double a, double b) {
            return base.Channel.AddNumAsync(a, b);
        }
        
        public double SubNum(double a, double b) {
            return base.Channel.SubNum(a, b);
        }
        
        public System.Threading.Tasks.Task<double> SubNumAsync(double a, double b) {
            return base.Channel.SubNumAsync(a, b);
        }
        
        public double MulNum(double a, double b) {
            return base.Channel.MulNum(a, b);
        }
        
        public System.Threading.Tasks.Task<double> MulNumAsync(double a, double b) {
            return base.Channel.MulNumAsync(a, b);
        }
        
        public double DivNum(double a, double b) {
            return base.Channel.DivNum(a, b);
        }
        
        public System.Threading.Tasks.Task<double> DivNumAsync(double a, double b) {
            return base.Channel.DivNumAsync(a, b);
        }
    }
}
