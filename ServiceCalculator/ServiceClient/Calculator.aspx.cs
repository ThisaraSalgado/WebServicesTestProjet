﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ServiceClient
{
    public partial class Calculator : System.Web.UI.Page
    {
        double num1, num2, answer;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void AddBtn_Click(object sender, EventArgs e)
        {
            CalculatorService.CalculatorServiceClient client = new CalculatorService.CalculatorServiceClient();
            num1 = Convert.ToDouble(Number1TextBox.Text);
            num2 = Convert.ToDouble(Number2TextBox.Text);
            answer= client.AddNum(num1,num2);
            AnswerTextBox.Text = answer.ToString();
        }

        

        protected void SubBtn_Click1(object sender, EventArgs e)
        {
            CalculatorService.CalculatorServiceClient client = new CalculatorService.CalculatorServiceClient();
            num1 = Convert.ToDouble(Number1TextBox.Text);
            num2 = Convert.ToDouble(Number2TextBox.Text);
            answer = client.SubNum(num1, num2);
            AnswerTextBox.Text = answer.ToString();
        }

        protected void DivBtn_Click1(object sender, EventArgs e)
        {
            CalculatorService.CalculatorServiceClient client = new CalculatorService.CalculatorServiceClient();
            num1 = Convert.ToDouble(Number1TextBox.Text);
            num2 = Convert.ToDouble(Number2TextBox.Text);
            answer = client.DivNum(num1, num2);
            AnswerTextBox.Text = answer.ToString();
        }

        protected void MulBtn_Click1(object sender, EventArgs e)
        {
            CalculatorService.CalculatorServiceClient client = new CalculatorService.CalculatorServiceClient();
            num1 = Convert.ToDouble(Number1TextBox.Text);
            num2 = Convert.ToDouble(Number2TextBox.Text);
            answer = client.MulNum(num1, num2);
            AnswerTextBox.Text = answer.ToString();
        }

       

       
    }
}