﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="ServiceClient.Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Calculator</title>
  <!--<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="Styles/bootstrap.min.css" type="text/css" />
  <link href="Styles/CustomStyles.css" rel="stylesheet" type="text/css" />
  <script src="Scripts/jquery-3.1.0.js"></script>
  <script src="Scripts/bootstrap.min.js"></script>-->
  <meta charset="utf-8">
  
  <link rel="stylesheet" href="Styles/bootstrap.min.css">
  <link href="Styles/CustomStyles.css" rel="stylesheet" type="text/css" /
  <script src="Scripts/jquery-1.4.1.min.js"></script>
  <script src="Scripts/bootstrap.min.js"></script>
</head>
<body class="body-image">
    <form id="form1" runat="server">
  
    <div class="container">
        <div class="jumbotron text-center">

        <h1>My First WCF Client</h1>
        <p>This is for practice web services</p>
        </div>
    </div>
  
    <div class="container">
        <div class="row">
            <div class="col-md-4" style="background-color:lavender;">
                <p></p>
                <p><span class="label label-success">Enter Number 1</span><span>  </span>
                    <asp:TextBox ID="Number1TextBox" runat="server" style="border-radius:8px;" 
                            class="form-control number-textbox" placeholder="Number 1"></asp:TextBox>
                </p>
                <p><span class="label label-success">Enter Number 2</span><span>  </span>
                    <asp:TextBox ID="Number2TextBox" runat="server" style="border-radius:8px;" 
                            class="form-control number-textbox" placeholder="Number 2"></asp:TextBox>
                </p>
            </div>
            <div class="col-md-4" style="background-color:lavenderblush;">
                <p style="text-align:center;">Perform Operation</p>
                <span><asp:Button ID="AddBtn" runat="server" Text="ADD" 
                            class="expand-item-btn login-btn" OnClick="AddBtn_Click" /></span>
                <span><asp:Button ID="SubBtn" runat="server" Text="SUB" 
                            class="expand-item-btn login-btn" OnClick="SubBtn_Click1"/></span>
                <span><asp:Button ID="MulBtn" runat="server" Text="MUL" 
                            class="expand-item-btn login-btn" OnClick="MulBtn_Click1" /></span>
                <span><asp:Button ID="DivBtn" runat="server" Text="DIV" 
                            class="expand-item-btn login-btn" OnClick="DivBtn_Click1"/></span>
            </div>
            <div class="col-md-4" style="background-color:lavender;">
                 <p></p>
                <p><span class="label label-success" style="align-content:center">Answer</span><span>  </span>
                    <asp:TextBox ID="AnswerTextBox" runat="server" style="border-radius:8px;" 
                            class="form-control number-textbox"></asp:TextBox>
                </p>
            </div>
        </div>
    </div>
    
    </form>
</body>
</html>
