﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceCalculator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CalculatorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CalculatorService.svc or CalculatorService.svc.cs at the Solution Explorer and start debugging.
    public class CalculatorService : ICalculatorService
    {
        double c;
        public double AddNum(double a, double b)
        {
            try
            {
                c = a + b;
            }
            catch
            {

            }
            return c;
        }

        public double SubNum(double a, double b)
        {
            try
            {
                c = a-b;
            }
            catch
            {

            }
            return c;
        }

        public double MulNum(double a, double b)
        {
            try
            {
                c = a*b;
            }
            catch
            {

            }
            return c;
        }

        public double DivNum(double a, double b)
        {
            try
            {
                c = a/b;
            }
            catch
            {

            }
            return c;
        }
    }
}
