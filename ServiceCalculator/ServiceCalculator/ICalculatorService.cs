﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceCalculator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICalculatorService" in both code and config file together.
    [ServiceContract]
    public interface ICalculatorService
    {
        [OperationContract]
        double AddNum(double a, double b);

        [OperationContract]
        double SubNum(double a, double b);

        [OperationContract]
        double MulNum(double a, double b);

        [OperationContract]
        double DivNum(double a, double b);
    }
}
